'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')
const Helpers = use('Helpers')

Route.get('/', () => {
  return { greeting: 'THIS IS SPARTA' }
})

Route.group(() => {
  Route.post('/register',   'AuthController.register')
  Route.post('/login',      'AuthController.login')

  Route.get('/struct/:model',       'GerencialController.getStruct')
  Route.get('/struct-list/:model',  'GerencialController.getListStruct')
  Route.get('/wizard/:model',       'GerencialController.getWizard')

  Route.get('/:model/finalizado',      'GerencialController.getPedidosFinalizados')

  Route.get('/:model',                  'GerencialController.get')
  Route.get('/:model/:id',              'GerencialController.getSingle')

  Route.post('/Imagem',                 'GerencialController.saveImagem')
  Route.post('/:model',                 'GerencialController.post')
  Route.post('/:model/:modelId/Image',  'GerencialController.image')

  Route.delete('/:model/:modelId', 'GerencialController.delete')
}).prefix('v1/gerencial')

Route.group(() => {
  Route.post('/register',   'AuthController.register')
  Route.post('/login',      'AuthController.login')

  Route.post('/Pagamento/finaliza',       'PagamentoController.site')
  Route.post('/Pagamento/getInstallments','PagamentoController.getInstallments')

  Route.get('/:model',             'SiteController.get')
  Route.get('/:model/:id',         'SiteController.getSingle')
  Route.get('/:model/slug/:slug',  'SiteController.getSingleSlug')
  Route.get('/:model/token/:token','SiteController.getSingleToken')

  Route.post('/:model/create',     'SiteController.create')
  Route.post('/:model/addProduto', 'SiteController.addProduto')
  Route.post('/:model/addLead',    'SiteController.addLead')
  Route.post('/:model/addEndereco',       'SiteController.addEndereco')
  Route.post('/:model/addFrete',          'SiteController.addFrete')
  Route.post('/:model/:id',           'SiteController.put')

  Route.delete('/:model/removeProduto',   'SiteController.removeProduto')
  Route.get('/:model/carrinho/:token',    'SiteController.getCarrinho')
}).prefix('v1/site')
