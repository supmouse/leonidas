'use strict'

const Database = use('Database')

class Struct {
    static async validate(modelStruct, model){
        const result = []
        const forcedConditions = model.forceConditions

        modelStruct.forEach(async element => {
            if (element.Field.startsWith('id_') && element.Key != 'PRI') {

                  result.push({
                    'nome': element.Field,
                    'label': element.Field.split('_')[element.Field.split('_').length - 1],
                    'object': element.Field.split('id_')[1],
                    'tipo': element.Field.endsWith('imagem') ? 'image' : 'relation',
                    'required': element.Null === "NO" ? true : false
                  })

            } else if (element.Field === 'hexa') {
              result.push({
                'nome': element.Field,
                'tipo': 'hexa',
                'label': 'cor',
                'required': element.Null === "NO" ? true : false
              })
            } else if (element.Field.startsWith('b_')) {
              result.push({
                'nome': element.Field,
                'tipo': 'bool',
                'label': element.Field.split('_')[element.Field.split('_').length - 1],
                'required': element.Null === "NO" ? true : false
              })
            } else if (element.Field.startsWith('dt_')){
              result.push({
                'nome': element.Field,
                'tipo': 'date',
                'required': element.Null === "NO" ? true : false
              })
            } else if (element.Type.startsWith('varchar')) {
              result.push({
                'nome': element.Field,
                'label': element.Field.split('_')[element.Field.split('_').length - 1],
                'tipo': forcedConditions && forcedConditions[element.Field] ? forcedConditions[element.Field] : 'text',
                'required': element.Null === "NO" ? true : false,
                'maxlength': element.Type.replace('varchar(','').replace(')','')
              })
            } else if (element.Type.startsWith('decimal')) {
              result.push({
                'nome': element.Field,
                'label': element.Field.split('_')[element.Field.split('_').length - 1],
                'tipo': 'decimal',
                'required': element.Null === "NO" ? true : false,
                'mask': '9999,99'
              })
            } else if (element.Type.startsWith('text') || element.Type.startsWith('longtext')) {
              result.push({
                'nome': element.Field,
                'label': element.Field.split('_')[element.Field.split('_').length - 1],
                'tipo': 'long_text',
                'required': element.Null === "NO" ? true : false
              })
            } else if (element.Type.startsWith('enum')) {
              const values = element.Type.replaceAll('enum(','').replaceAll(')','').replaceAll('\'','')

              result.push({
                'nome': element.Field,
                'label': element.Field.split('_')[element.Field.split('_').length - 1],
                'tipo': 'enum',
                'values': values.split(','),
                'required': element.Null === "NO" ? true : false
              })
            } else if (!element.Field.endsWith('_at') && element.Key != 'PRI') {
              result.push({
                'nome': element.Field,
                'tipo': 'text',
                'required': element.Null === "NO" ? true : false
              })
            }
        });

        return result
    }

    static async getList(model){
      const listOptions = model.listOptions

      return listOptions
    }

    static async get(model){

        const modelStruct = await Database.raw('SHOW COLUMNS FROM '+model.table)
        const result = this.validate(modelStruct[0], model)

        return result
    }
}

module.exports = Struct
