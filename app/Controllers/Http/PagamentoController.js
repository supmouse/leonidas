'use strict'

const Pedido = use('App/Models/Pedido');
const Pagamento = use('App/Models/Pagamento');
const GatewayController = use('App/Controllers/GatewayController');
const Pagarme = require('pagarme')

class PagamentoController {



  async get({request, response}){}

  async getInstallments({request, response}){
    try {
      const p = request.body
      const pedido = await Pedido.findToken(p.token)
      const pagarme = await Pagarme.client.connect({ api_key: 'ak_test_7cVdmTgFHyyijk2N3kjg5ZKA3Rbxyg' })

      const calculaparcelas = await pagarme.transactions.calculateInstallmentsAmount({
        'amount' : pedido.total(),
        'free_installments' : 3,
        'max_installments' : 3,
        'interest_rate' : 0
      });

      return calculaparcelas
    } catch (e) {
      console.log(e)
    }
  }

  async boleto(pedido){
    try {

      const pagamento = new Pagamento()
      const pagarmeData = await this.getJson(pedido, null)
      const pagarme = await Pagarme.client.connect({ api_key: 'ak_test_7cVdmTgFHyyijk2N3kjg5ZKA3Rbxyg' })
      const ped = await Pedido.find(pedido.id)

      if (pagarme) {
        let pagamentoPagarme = await pagarme.transactions.create(pagarmeData)

        if (pagamentoPagarme) {

            pagamento.tipo = "pagarme"
            pagamento.id_externo = pagamentoPagarme.id
            pagamento.status = "aguardando pagamento"
            pagamento.metodo = 'boleto'
            pagamento.valor = pagamentoPagarme.amount
            pagamento.link = pagamentoPagarme.boleto_url
            await pagamento.save();

            ped.id_ec_pagamento = pagamento.id
            ped.situacao = "FINALIZADO"
            ped.status = "EM PROCESSAMENTO"
            await ped.save()

            return pedido
        } else {
          return null
        }
      }

	  } catch (e) {
		  console.log(e)
      if (e.response.errors){
        console.log(e.response.errors)
      }
      return null
	  }
  }

  async credito(pedido, p){
    try {

      const gateaway = new GatewayController(pedido, p)

      const pagamento = new Pagamento()
      const pagarmeData = await this.getJson(pedido, p)
      const pagarme = await Pagarme.client.connect({ api_key: 'ak_test_7cVdmTgFHyyijk2N3kjg5ZKA3Rbxyg' })
      const ped = await Pedido.find(pedido.id)

      if (pagarme) {
        let pagamentoPagarme = await pagarme.transactions.create(pagarmeData)

        if (pagamentoPagarme.status == "paid") {

            pagamento.tipo = "pagarme"
            pagamento.id_externo = pagamentoPagarme.id
            pagamento.status = pagamentoPagarme.status
            pagamento.bandeira = pagamentoPagarme.card_brand
            pagamento.metodo = 'credito'
            pagamento.valor = pagamentoPagarme.amount
            pagamento.parcelas = pagamentoPagarme.installments
            await pagamento.save();

            ped.id_ec_pagamento = pagamento.id
            ped.situacao = "FINALIZADO"
            ped.status = "EM PROCESSAMENTO"
            await ped.save()

            return pedido

        } else {
          return null
        }
      }

	  } catch (e) {
		  console.log(e)
      if (e.response.errors){
        console.log(e.response.errors)
      }
      return null
	  }
  }

  async takeaway(pedido){
    try {
      const pagamento = new Pagamento()
      const ped = await Pedido.findToken(pedido.token)

      pagamento.tipo = "offline"
      pagamento.status = "aguardando pagamento"
      pagamento.metodo = 'takeaway'
      pagamento.valor = ped.total()
      await pagamento.save()

      ped.id_ec_pagamento = pagamento.id
      ped.situacao = "FINALIZADO"
      ped.status = "EM PROCESSAMENTO"
      await ped.save()

      return pedido
    } catch (e) {
      console.log(e)

      return null
    }
  }

  async offline(pedido){
    try {
      const pagamento = new Pagamento()
      const ped = await Pedido.findToken(pedido.token)

      pagamento.tipo = "offline"
      pagamento.status = "aguardando pagamento"
      pagamento.metodo = 'delivery'
      pagamento.valor = ped.total()
      await pagamento.save()

      ped.id_ec_pagamento = pagamento.id
      ped.situacao = "FINALIZADO"
      ped.status = "EM PROCESSAMENTO"
      await ped.save()

      return pedido
    } catch (e) {
      console.log(e)

      return null
    }
  }

  async site({request, response}){
      const p = request.body
      const pedido = await Pedido.findToken(p.token)

      let pagamento = null

      if(p.tipo === "credito") pagamento = await this.credito(pedido.toJSON(), p)
      else if(p.tipo === "boleto") pagamento = await this.boleto(pedido.toJSON())
      else if(p.tipo === "takeaway") pagamento = await this.takeaway(pedido.toJSON())
      else if(p.tipo === "offline") pagamento = await this.offline(pedido.toJSON())

      if (pagamento) {
        return pagamento
      } else {
        response
        .status(500)
        .send('Erro ao realizar o pagamento')
      }
  }

}
module.exports = PagamentoController
