'use strict'

const User = use('App/Models/User');
const Perfil = use('App/Models/Perfil');
const Loja = use('App/Models/Loja');

class AuthController {

  async register({request, auth, response}) {

    try {
      let perfil = await Perfil.create({
        nome: request.body.nome,
        sobrenome: request.body.sobrenome,
        b_termos: request.body.termos
      })

      let loja = await Loja.create({
        nome:request.body.loja,
      })

      let user = await User.create({
        email: request.body.email,
        password: request.body.password,
        id_usr_perfil: perfil.id,
        id_ec_loja:loja.id,
        b_ativo: 1
      })

      

      let token = await auth.generate(user)

      const data = {
        id: user.id,
        plano: user.plano,
        ativo: user.b_ativo,
        admin: user.b_admin,
        nivel: user.id_sc_nivel,
        loja: user.id_ec_loja ? await user.getLoja().fetch() : null,
        perfil: user.id_usr_perfil ? await user.getPerfil().fetch() : null,
        token: token
      }

      return response.json(data)

    } catch (e) {
      console.log(e)
      return response.json({error: true, msg: "nao foi possiver cirar o usuario"})
    }
  }

  async login({request, auth, response}) {

    let {email, password} = request.all();

    try {
      if (await auth.attempt(email, password)) {
        let user = await User.findBy('email', email)
        let token = await auth.generate(user)

        Object.assign(user, token)

        const data = {
          id: user.id,
          plano: user.plano,
          ativo: user.b_ativo,
          admin: user.b_admin,
          nivel: user.id_sc_nivel,
          loja: user.id_ec_loja ? await user.getLoja().with('imagem').fetch() : null,
          perfil: user.id_usr_perfil ? await user.getPerfil().fetch() : null,
          token: token
        }

        return response.json(data)
      }


    }
    catch (e) {
      console.log(e)
      return response.json({message: 'You are not registered!'})
    }
  }
  async getPosts({request, response}) {
    let posts = await Post.query().with('user').fetch()

    return response.json(posts)
  }

}

module.exports = AuthController
