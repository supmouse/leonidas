'use strict'

const Database = use('Database')
const Struct = use('App/Helpers/Struct')
const Helpers = use('Helpers')
const Tooltip = use('App/Models/Tooltip')

const { exec } = require('child_process')
const fs = require('fs')

class BaseController {
  async getNestStruct(reference) {
    const o = reference.split("_")
    const r = o[1].charAt(0).toUpperCase() + o[1].slice(1)
    const m = use('App/Models/'+r)

    return await Struct.get(m)
  }

  async getNestOptions(reference) {
    const o = reference.split("_")
    const r = o.length > 1 ? o[0].charAt(0).toUpperCase() + o[0].slice(1) + o[1].charAt(0).toUpperCase() + o[1].slice(1) : o[0].charAt(0).toUpperCase() + o[0].slice(1)
    const m = use('App/Models/'+r)

    return await m.getOptions()
  }

  async getMTMStruct(relation) {
    const m = use('App/Models/'+relation.model)
    const s = await Struct.get(m)
    const o = await m.getOptions(false)

    return {
      "nome": relation.nome,
      "label": relation.label,
      "object": relation.object,
      "tipo": relation.label === 'imagem' ? "dropzone" : "multi_relation",
      "struct": s,
      "options":o
    }

  }

  async getListStruct(request){
    const model = use('App/Models/'+request.params.model)
    const modelStructList = await Struct.getList(model)

    return modelStructList
  }

  async get(request){
    const model = use('App/Models/'+request.params.model)
    const page = request.request.get('page')
    const filtro = request.request.get('filtro')
    const results = await model.get(page, filtro.filtro ? filtro.filtro : null)
    const modelStructList = await Struct.getList(model)

    for (let i in results.data) {
      for (let j in modelStructList) {
        if (modelStructList[j].obj) {
          let obj = await model.find(results.data[i].id)
          let objItems = await obj[modelStructList[j].obj]().fetch()
          results.data[i][modelStructList[j].nome] = objItems ? objItems.toJSON() : null
        }
      }
    }

    return results
  }

  async getPedidosFinalizados(request){
    const model = use('App/Models/'+request.params.model)
    const page = request.request.get('page')
    const results = await model.query().where('situacao','FINALIZADO').fetch()
    const modelStructList = await Struct.getList(model)

    for (let i in results.data) {
      for (let j in modelStructList) {
        if (modelStructList[j].obj) {
          let obj = await model.find(results.data[i].id)
          let objItems = await obj[modelStructList[j].obj]().fetch()
          results.data[i][modelStructList[j].nome] = objItems ? objItems.toJSON() : null
        }
      }
    }

    return results
  }

  async getSingle(request){
    const model = use('App/Models/'+request.params.model)
    const result = typeof model.getSingle === 'function' ? await model.getSingle(request.params.id) : await model.find(request.params.id)
    const relations = model.getRelations
    const manyToMany = model.getManyToMany
    const value = result.toJSON()

    if(typeof model.getSingle != 'function') {
      if (relations) {
        for (var r in relations) {
            let rellll = await result[relations[r]]().fetch()
            value[relations[r]] = rellll
        }
      }

      if (manyToMany) {
        for (var r in manyToMany) {
            let rellll = await result[manyToMany[r].label]().fetch()
            value[manyToMany[r].label] = rellll
        }
      }
    }

    return value
  }

  async post(request){
    const model = use('App/Models/'+request.params.model)
    const o = request.request.body.items
    const r = request.request.body.relations

    const obj = 'id' in o ? await model.find(o.id) : new model()

    try {
      for(var i  in  o) {
        obj[i] = i == "valor" ? o[i].replace(',','.') : o[i]
      }

      await obj.save()

      for(var j  in  r) {
        let name = j.split("_")
        name = name[name.length - 1]

        await obj[name]().detach()
        await obj[name]().attach(r[j])
      }
    } catch (e){
      console.log(e)
    }

    return obj
  }

  async image(request){
    try {
      const model = use('App/Models/'+request.params.model)
      const image = use('App/Models/Imagem')
      const productId = request.params.modelId

      const file = request.request.file('file', {
        types: ['image'],
        size: '10mb'
      })

      const finalName = productId+"-"+file.clientName
      const targetPath = Helpers.appRoot()+"/upload/produtos/"

      const obj = await model.find(productId)
      const imagem = new image()

      await file.move(targetPath, {
        name: finalName,
        overwrite: true
      })

      exec('jpegoptim --size=250k '+targetPath+finalName, (err, stdout, stderr) => {});

      imagem.imagem = "/upload/produtos/"+finalName
      await imagem.save()

      obj.imagem().attach([imagem.id])

      return imagem

    }catch (e){
      console.log(e)
    }

    return obj
  }

  async getStruct(request){
      const model = use('App/Models/'+request.params.model)
      const modelStruct = await Struct.get(model)
      const mtm = model.getManyToMany

      for(var i  in  modelStruct) {
        if (modelStruct[i].tipo === "relation"){
          modelStruct[i].options = await this.getNestOptions(modelStruct[i].object)

          if (modelStruct[i].nome === "id_categoria"){
            modelStruct[i].label = "Escolha a categoria mãe"
          }
        }

        const tooltip = await Tooltip.getInfo(model.table, modelStruct[i].nome)
        if (tooltip) modelStruct[i].tooltip = tooltip.descricao
      }

      

      if (mtm){
        for(var j  in  mtm) {
          modelStruct.push(await this.getMTMStruct(mtm[j]))
        }
      }

      return modelStruct
  }

  async delete(request){
    const model = use('App/Models/'+request.params.model)
    const obj = await model.find(request.params.modelId)
    const data = new Date()
    const dataFormatada = (data.getFullYear() + "-" + (data.getMonth() + 1) + "-" + data.getDate() + " " + data.getHours() + ":" + data.getMinutes() + ":" + data.getSeconds())

    obj.deleted_at = dataFormatada
    await obj.save()
  }

  async getWizard(request){
    const model = use('App/Models/'+request.params.model)
    const modelStruct = await Struct.get(model)
    const wizard = model.getWizard

    return wizard
  }

  async createFile(label, name, value){
    let ext = value.split(";")
    ext = ext[0].split("/")
    const realname = "upload/"+name.replace("/ /g","_")+"_"+label+"."+ext[1]
    const base64Image = value.split(';base64,').pop();

    try {
      const val = await fs.writeFile(Helpers.appRoot()+"/"+realname, base64Image, {encoding: 'base64'}, function(err) {
        console.log(err)
      });
    } catch (e){
      console.log(e)
    }

    return "/"+realname
  }

  async saveImagem(request){
    const Imagem = use('App/Models/Imagem')
    let r = null

    try{
      const file = request.request.file('imagem', {
        types: ['image'],
        size: '10mb'
      })

      const finalName = file.clientName.replaceAll(/\s/g,"_")
      const targetPath = Helpers.appRoot()+"/upload/"

      const imagem = new Imagem()

      await file.move(targetPath, {
        name: finalName,
        overwrite: true
      })

      exec('jpegoptim --size=250k '+targetPath+finalName, (err, stdout, stderr) => {});

      imagem.imagem = "/upload/"+finalName
      imagem.nome = finalName
      r = await imagem.save()
    } catch (e){
      console.log(e)
    }

    return r
  }
}

module.exports = BaseController
