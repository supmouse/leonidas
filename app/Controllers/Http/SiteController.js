'use strict'

class BaseController {

  async get(request){
    const model = use('App/Models/'+request.params.model)
    const params = request.request.all()
    const value = await model.getSite(params.page ? params.page : 1, params.filtro ? params.filtro : null)

    return value
  }

  async getSingle(request){
    const model = use('App/Models/'+request.params.model)
    const result = await model.getSingle(request.params.id)
    const value = result.toJSON()

    return value
  }

  async getSingleSlug(request){
    const model = use('App/Models/'+request.params.model)
    const result = await model.findSlug(request.params.slug)
    const relations = model.getRelations
    const manyToMany = model.getManyToMany
    const value = result.toJSON()

    if (relations) {
      for (var r in relations) {
          let rellll = await result[relations[r]]().fetch()
          value[manyToMany[r]] = rellll
      }
    }

    if (manyToMany) {
      for (var r in manyToMany) {
          let rellll = await result[manyToMany[r].label]().fetch()
          value[manyToMany[r].label] = rellll
      }
    }

    return value
  }

  async getSingleToken(request){
    const model = use('App/Models/'+request.params.model)
    const result = await model.findToken(request.params.token)
    const relations = model.getRelations
    const manyToMany = model.getManyToMany
    const value = result.toJSON()

    if (relations) {
      for (var r in relations) {
          let rellll = await result[relations[r]]().fetch()
          value[manyToMany[r]] = rellll
      }
    }

    if (manyToMany) {
      for (var r in manyToMany) {
          let rellll = await result[manyToMany[r].label]().fetch()
          value[manyToMany[r].label] = rellll
      }
    }

    return value
  }

  async put (request){
    try {
      const model = use('App/Models/'+request.params.model)
      const o = request.request.body
      const result = await model.find(o.id)

      for(var i in  o) {
        result[i] = o[i]
      }

      await result.save()

      return result
    } catch (e) {
      console.log(e)
    }
  }

  async create({request}){
    try {
      const model = use('App/Models/'+request.params.model)
      const result = await model.create(request.post())

      return result
    } catch (e) {
      console.log(e)
    }
  }

  async addProduto({request}){
	  try {
      const model = use('App/Models/'+request.params.model)
      const result = await model.addProduto(request.post())

      return result
	  } catch (e) {
		  console.log(e)
	  }
  }

  async addLead({request}){
	  try {
      const model = use('App/Models/'+request.params.model)
      const result = await model.addLead(request.post())

      return result
	  } catch (e) {
		  console.log(e)
	  }
  }

  async addEndereco({request}){
    try {
      const model = use('App/Models/'+request.params.model)
      const result = await model.addEndereco(request.post())

      return result
    } catch (e) {
      console.log(e)
    }
  }

  async addFrete({request}){
    try {
      const model = use('App/Models/'+request.params.model)
      const result = await model.addFrete(request.post())

      return result
    } catch (e) {
      console.log(e)
    }
  }

  async removeProduto({request}){
	  try {
      const model = use('App/Models/'+request.params.model)
      const result = await model.removeProduto(request.post())

      return result
	  } catch (e) {
		  console.log(e)
	  }
  }

  async getCarrinho({request}){
	  try {
      const model = use('App/Models/'+request.params.model)
      const result = await model.getCarrinho(request.params.token)

      return result
	  } catch (e) {
		  console.log(e)
	  }
  }
}

module.exports = BaseController
