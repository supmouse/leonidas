'use strict'

const User = use('App/Models/User');
const Perfil = use('App/Models/Perfil');
const mercadopago = require('mercadopago');

class GatewayController {

  async getJsonPagarme(pedido, cartao = null){
    const produtos = Array();
    const user = pedido.usuario ? pedido.usuario : pedido.lead
    const tel = user.perfil.telefone.replaceAll(" ","").replaceAll("(","").replaceAll(")","").replaceAll("-","")
    let total = 0

    pedido.produto.map(produto => {
      const valor = (produto.valor - (produto.valor * produto.desconto/100))
      total = total + valor
      produtos.push(
          {
          'id' :  produto.id.toString(),
          'title' :  produto.nome,
          'unit_price' : valor*100,
          'quantity' :  '1',
          'tangible' : true
          }
      )
    })

    const data = {
          'amount' : (total + pedido.frete.valor) * 100,
          'customer' :{
              'external_id' : user.id.toString(),
              'name' : user.perfil.nome,
              'type' : 'individual',
              'country' : 'br',
              'documents' : [
                  {
                    'type' : 'cpf',
                    'number' : user.perfil.cpf.replace(/-/g,"")
                  }
              ],
              'phone_numbers' : [ tel ],
              'email' : user.email
          },
          'billing' : {
            'name' : user.perfil.nome,
            'address' :{
              'country' : 'br',
              'street' : pedido.endereco.rua,
              'street_number' : pedido.endereco.numero.toString(),
              'state' : pedido.endereco.cidade.estado.sigla,
              'city' : pedido.endereco.cidade.cidade,
              'neighborhood' : pedido.endereco.bairro ? pedido.endereco.bairro : pedido.endereco.cidade.cidade,
              'zipcode' : pedido.endereco.cep.replace(/-/g,"")
            }
           },
          'shipping' :{
              'name' : user.perfil.nome,
              'fee' : 0,
              'delivery_date' : "2019-02-20", //$pedido->transporte->dt_entrega,
              'expedited' : false,
              'address' : {
                'country' : 'br',
                'street' : pedido.endereco.rua,
                'street_number' : pedido.endereco.numero.toString(),
                'state' : pedido.endereco.cidade.estado.sigla,
                'city' : pedido.endereco.cidade.cidade,
                'neighborhood' : pedido.endereco.bairro,
                'zipcode' : pedido.endereco.cep.replace(/-/g,"")
              }
            },
          'items' : produtos
    }

    if (cartao) {
      data.installments = cartao['parcela']
      data.payment_method = 'credit_card'
      data.card_holder_name = cartao['nome_cartao']
      data.card_cvv = cartao['cvv']
      data.card_number = cartao['numero_cartao'].replace(/\./g,"")
      data.card_expiration_date = cartao['dt_expiracao'].replace(/\//g,"")
    } else {
      const expiracao = new Date()
      expiracao.setDate(expiracao.getDate() + 3)
      const day = expiracao.getFullYear()
      const month = expiracao.getMonth() + 1
      const year = expiracao.getDate()

      data.payment_method = 'boleto'
      data.boleto_expiration_date = day+"-"+month+"-"+year
    }

    return data
  }

  async getJsonMercadoPago(pedido, cartao = null){
    var payment_data = {
      transaction_amount: Number(req.body.transactionAmount),
      token: req.body.token,
      description: req.body.description,
      installments: Number(cartao['parcela']),
      payment_method_id: req.body.paymentMethodId,
      issuer_id: req.body.issuer,
      payer: {
        email: req.body.email,
        identification: {
          type: req.body.docType,
          number: req.body.docNumber
        }
      }
    }

    return data
  }

  async pagarme() {
    try {

      const pagarmeData = await this.getJsonPagarme(pedido, p)
      const pagarme = await Pagarme.client.connect({ api_key: 'ak_test_7cVdmTgFHyyijk2N3kjg5ZKA3Rbxyg' })
      const ped = await Pedido.find(pedido.id)

      if (pagarme) {
        return await pagarme.transactions.create(pagarmeData)
      } else {
        return null
      }

    } catch (e) {
      console.log(e)
      if (e.response.errors){
        console.log(e.response.errors)
      }
      return null
    }
  }

  async mercadoPago() {
    try {

      mercadopago.configurations.setAccessToken("YOUR_ACCESS_TOKEN");
      const pagarmeData = await this.getJsonPagarme(pedido, p)
      return await mercadopago.payment.save(pagarmeData)

    } catch (e) {
      console.log(e)
      if (e.response.errors){
        console.log(e.response.errors)
      }
      return null
    }
  }

}
