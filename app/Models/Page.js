'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Page extends Model {
    static get table () {
        return 'ed_pages'
    }

    static get hidden () {
      return ['updated_at','created_at','deleted_at']
    }

    static get deleteTimestamp () {
      return 'deleted_at'
    }

    static get listOptions () {
      return [{
                "nome": "id",
                "label": "#",
              },{
                "nome": "nome",
                "label": "Nome",
              },{
                "nome": "acoes",
                "label": "Ações",
                "values":[
                  {acao: "editar", label: "Editar Page"},
                  {acao: "excluir", label: "Excluir Page"}
                ]
              }]
    }

    static label() {
      return this.nome
    }

    static async getValues(id){
        return await this.query().whereNull('deleted_at').fetch()
    }

    static async get(page = 1){
      let valor = await this.query().whereNull('deleted_at').paginate(page ? page.page : 1, 10)
      valor = valor.toJSON()

      return valor
    }

    static async getSite(){
      let valor = await this.query().whereNull('deleted_at').fetch()
      valor = valor.toJSON()
      return valor
    }

    static async findSlug(value){
      return await this.query().where('slug', value).whereNull('deleted_at').first()
    }

    static async getOptions(){
      const items = await this.query().whereNull('deleted_at').fetch()
      const serialize = items.toJSON()

      return serialize.map( i => { return ( {id: i.id, label:i.nome} ) } )
    }
}

module.exports = Page
