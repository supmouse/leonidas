'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const Perfil = use('App/Models/Perfil')

class Lead extends Model {
    static get table () {
        return 'usr_leads'
    }

    static get hidden () {
      return ['updated_at','created_at','deleted_at']
    }

    perfil () {
      return this.belongsTo('App/Models/Perfil', 'id_usr_perfil', 'id')
    }

    static async get(){
      const d = new Date()

      const result = await this.query().whereRaw('YEAR(created_at) = ?',d.getFullYear()).whereRaw('MONTH(created_at) = ?',d.getMonth()+1).fetch()
      return result.toJSON()
    }

    static async create(params) {
      let lead = await Lead.findBy('email',params.email)
      let perfil = await Perfil.findBy('cpf',params.cpf)
      if(!params.cpf) perfil = await Perfil.findBy('ci',params.ci)

      lead = lead ? lead : new Lead()
      lead.email = params.email,
      lead.tipo = 'site'
      await lead.save()

      perfil = perfil ? perfil : new Perfil()
      perfil.nome = params.nome
      perfil.telefone = params.telefone

      if(params.cpf) perfil.cpf = params.cpf
      else perfil.ci = params.ci

      await perfil.save()

      await lead.perfil().associate(perfil)

      return lead
    }
}

module.exports = Lead
