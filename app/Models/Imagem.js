'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Imagem extends Model {
    static get table () {
        return 'gb_imagens'
    }

    static get hidden () {
      return ['updated_at','created_at','deleted_at']
    }

    static get forceConditions () {
      const arr = []
      arr['imagem'] = 'imagem'
      arr['imagem_mobile'] = 'imagem'

      return arr
    }

    produto () {
      return this.belongsToMany('pdr_produtos_has_gb_imagens','id','id_gb_imagem')
    }

    static get listOptions () {
      return [{
                "nome": "id",
                "label": "#",
              },{
                "nome": "nome",
                "label": "Nome",
              },{
                "nome": "imagem",
                "label": "Imagem",
              },{
                "nome": "acoes",
                "label": "Ações",
                "values":[
                  {acao: "editar", label: "Editar Imagem"},
                  {acao: "excluir", label: "Excluir Imagem"}
                ]
              }]
    }

    static async get(page = 1){
      const result = await this.query().whereNull('deleted_at').orderBy('id','DESC').paginate(page ? page.page : 1, 12)
      return result.toJSON()
    }

    static async getOptions(){
      const items = await this.all()
      const serialize = items.toJSON()

      return serialize.map( i => { return ( {id: i.id, label:i.nome} ) } )
    }
}

module.exports = Imagem
