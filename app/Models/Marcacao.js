'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Marcacao extends Model {
    static get table () {
        return 'srv_marcacoes'
    }

    static get hidden () {
      return ['updated_at','created_at','deleted_at']
    }

}

module.exports = Marcacao
