'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Servico extends Model {
    static get table () {
        return 'srv_servicos'
    }

    static get hidden () {
        return ['created_at', 'updated_at', 'deleted_at']
    }
}

module.exports = Servico
