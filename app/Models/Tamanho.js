'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Tamanho extends Model {
    static get table () {
        return 'prd_tamanhos'
    }

    static get hidden () {
      return ['updated_at','created_at','deleted_at']
    }

    static get deleteTimestamp () {
      return 'deleted_at'
    }

    produto () {
      return this.belongsToMany('pdr_produtos_has_prd_tamanhos','id','id_prd_tamanho')
    }

    static get listOptions () {
      return [{
                "nome": "id",
                "label": "#",
              },{
                "nome": "nome",
                "label": "Nome",
              },{
                "nome": "acoes",
                "label": "Ações",
                "values":[
                  {acao: "editar", label: "Editar Tamanho"},
                  {acao: "excluir", label: "Excluir Tamanho"}
                ]
              }]
    }

    static async get(page = 1){
      const result = await this.query().whereNull('deleted_at').orderBy('id','DESC').paginate(page ? page.page : 1, 10)
      return result.toJSON()
    }

    static async getSite(){
      return this.query().whereNull('deleted_at').fetch()
    }

    static async getOptions(){
      const items = await this.query().whereNull('deleted_at').fetch()
      const serialize = items.toJSON()

      return serialize.map( i => { return ( {id: i.id, label:i.nome} ) } )
    }
}

module.exports = Tamanho
