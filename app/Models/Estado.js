'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Estado extends Model {
    static get table () {
        return 'gb_estados'
    }

    static get hidden () {
      return ['updated_at','created_at','deleted_at']
    }

}

module.exports = Estado
