'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Cidade extends Model {
    static get table () {
        return 'gb_cidades'
    }

    static get hidden () {
      return ['updated_at','created_at','deleted_at']
    }

    estado () {
      return this.belongsTo('App/Models/Estado', 'id_gb_estado', 'id')
    }

}

module.exports = Cidade
