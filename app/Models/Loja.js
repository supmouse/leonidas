'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Lote extends Model {
  static get table () {
    return 'ec_lojas'
  }

  static get hidden () {
    return ['updated_at','created_at','deleted_at']
  }

  imagem () {
    return this.hasOne('App/Models/Imagem','id_gb_imagem','id')
  }
}

module.exports = Lote
