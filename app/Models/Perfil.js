'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Perfil extends Model {
  static get table () {
    return 'usr_perfis'
  }

  static get hidden () {
    return ['updated_at','created_at','deleted_at']
  }

  static async getSingle(value){
    return await this.query().where('id', value).whereNull('deleted_at').first()
  }

  static async getOptions(){
    const items = await this.query().whereNull('deleted_at').fetch()
    const serialize = items.toJSON()

    return serialize.map( i => { return ( {id: i.id, label:i.nome+" "+i.sobrenome} ) } )
  }

  static get(){
    return this.all()
  }
}

module.exports = Perfil
