'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Plano extends Model {
  static get table () {
    return 'sys_planos'
  }

  static get hidden () {
    return ['updated_at','created_at','deleted_at']
  }

  static get(){
    return this.all()
  }
}

module.exports = Plano
