'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Colecao extends Model {
    static get table () {
        return 'ec_clube'
    }

    static get hidden () {
      return ['updated_at','created_at','deleted_at']
    }

    static get deleteTimestamp () {
      return 'deleted_at'
    }

    static get listOptions () {
      return [{
                "nome": "id",
                "label": "#",
              },{
                "nome": "nome",
                "label": "Nome",
              },{
                "nome": "acoes",
                "label": "Ações",
                "values":[
                  {acao: "editar", label: "Editar Coleção"},
                  {acao: "excluir", label: "Excluir Coleção"}
                ]
              }]
    }

    imagem () {
      return this.belongsTo('App/Models/Imagem', 'id_gb_imagem', 'id')
    }

    static label() {
      return this.nome
    }

    static async get(page = 1){
      const result = await this.query().whereNull('deleted_at').paginate(page ? page.page : 1, 10)
      return result.toJSON()
    }

    static async getSite(page = 1, filtro){
      const base = this.query().with('imagem').whereNull('deleted_at')

      if (filtro === 'GOLD') base.where('nivel', 'GOLD')
      else if (filtro === 'SILVER') base.where('nivel', 'SILVER')
      else if (filtro === 'BRONZE') base.where('nivel', 'BRONZE')

      return base.paginate(page ? page.page : 1, 10)
    }

    static async getOptions(){
      const items = await this.query().whereNull('deleted_at').fetch()
      const serialize = items.toJSON()

      return serialize.map( i => { return ( {id: i.id, label:i.nome} ) } )
    }
}

module.exports = Colecao
