'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const crypto = require('crypto')
const Lead = use('App/Models/Lead')
const Endereco = use('App/Models/Endereco')
const Frete = use('App/Models/Frete')
const Pagamento = use('App/Models/Pagamento')

class Pedido extends Model {
    static get table () {
        return 'ec_pedidos'
    }

    static get hidden () {
      return ['updated_at','created_at','deleted_at']
    }

    static get getRelations (){
      const relations = [];
      //relations["id_ec_cupom"] = "cupom"
      relations["id_ec_frete"] = "frete"
      relations["id_ec_pagamento"] = "pagamento"
      relations["id_gb_endereco"] = "endereco"
      relations["id_usr_lead"] = "lead"
      //relations["id_usr_usuario"] = "usuario"

      return relations
    }

    static get getManyToMany (){
      return [{
                "nome": "id_prd_produto",
                "label": "produto",
                "object": "prd_produto",
                "model":"Produto"
              }]
    }

    total(){
      let total = 0
      const pedido = this.toJSON()

      pedido.produto.forEach(produto => {
        const valor = produto.valor - (produto.valor * produto.desconto/100)
        total = total + valor
      })

      total = total + pedido.frete.valor

      return total * 100
    }

    produto () {
      return this.belongsToMany('App/Models/Produto','id_ec_pedidos','id_prd_produtos','id','id').pivotTable('ec_pedidos_has_prd_produtos').withPivot(['id_prd_cor','id_prd_tamanho','quantidade'])
    }

    lead () {
      return this.belongsTo('App/Models/Lead', 'id_usr_lead', 'id')
    }

    endereco () {
      return this.belongsTo('App/Models/Endereco', 'id_gb_endereco', 'id')
    }

    frete () {
      return this.belongsTo('App/Models/Frete', 'id_ec_frete', 'id')
    }

    pagamento () {
      return this.belongsTo('App/Models/Pagamento', 'id_ec_pagamento', 'id')
    }

    static async findToken(value){
      return await this.query()
        .with('produto')
        .with('produto.imagem')
        .with('frete')
        .with('lead')
        .with('lead.perfil')
        .with('endereco')
        .with('endereco.cidade')
        .with('endereco.cidade.estado')
        .with('pagamento').where('token', value).whereNull('deleted_at').first()
    }

    static async getSingle(value){
      return await this.query()
        .with('produto')
        .with('produto.imagem')
        .with('frete')
        .with('lead')
        .with('lead.perfil')
        .with('endereco')
        .with('pagamento').where('id', value).whereNull('deleted_at').first()
    }

    static async create(params) {
      const current_date = (new Date()).valueOf().toString()
      const random = Math.random().toString()
      const token = crypto.createHash('sha1').update(current_date + random).digest('hex')

      const pedido = new Pedido()
      pedido.token = token
      pedido.situacao = "EM COMPRA"
      pedido.status = "INICIADO"
      await pedido.save()

      await pedido.produto().attach([params.produto.produto], (row) => {
          row.id_prd_cor = params.produto.id_prd_cor
          row.id_prd_tamanho = params.produto.id_prd_tamanho
          row.quantidade = params.produto.quantidade
      })
      return pedido
    }

    static async addProduto(params) {
      const pedido = await this.findToken(params.token)
      await pedido.produto().attach([params.produto.produto], (row) => {
          row.id_prd_cor = params.produto.id_prd_cor
          row.id_prd_tamanho = params.produto.id_prd_tamanho
          row.quantidade = params.produto.quantidade
      })

      return pedido
    }

    static async addLead(params) {
      const pedido = await this.findToken(params.token)
      const lead = await Lead.find(params.lead)

      await pedido.lead().associate(lead)

      return pedido
    }

    static async addEndereco(params) {
      const pedido = await this.findToken(params.token)
      const endereco = await Endereco.find(params.endereco)

      await pedido.endereco().associate(endereco)

      return pedido
    }

    static async addFrete(params) {
      const pedido = await this.findToken(params.token)
      const frete = await Frete.find(params.frete)

      await pedido.frete().associate(frete)

      return pedido
    }

    static async removeProduto(params) {
      const pedido = await this.findToken(params.token)
      await pedido.produto().detach([params.produto])

      return pedido
    }

    static async get(){
      const d = new Date()

      const result = await this.query().whereRaw('YEAR(created_at) = ?',d.getFullYear()).whereRaw('MONTH(created_at) = ?',d.getMonth()+1).fetch()
      return result.toJSON()
    }

    static async getCarrinho(token){
      let pedido = await this.query().with('produto.imagem').with('lead').with('endereco.cidade.estado').with('frete').with('pagamento').where('token', token).whereNull('deleted_at').first()
      let total = 0

      if (pedido) {
        pedido = pedido.toJSON()


        pedido.produto.forEach(item => {
          item.total = item.valor - (item.valor * item.desconto/100)
          total += item.total * item.pivot.quantidade
        })

        pedido.subtotal = total
        pedido.total = pedido.frete ? total + pedido.frete.valor : total

        return pedido
      } else {
        return null
      }
    }
}

module.exports = Pedido
