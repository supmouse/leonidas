'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Cor extends Model {
    static get table () {
        return 'prd_cores'
    }

    produtos () {
        return this.belongsToMany('pdr_produtos_has_prd_cores','id','id_prd_cor')
    }

    static get hidden () {
        return ['created_at', 'updated_at', 'deleted_at']
    }

    static get deleteTimestamp () {
      return 'deleted_at'
    }

    static get listOptions () {
      return [{
                "nome": "id",
                "label": "#",
              },{
                "nome": "nome",
                "label": "Nome",
              },{
                "nome": "hexa",
                "label": "Cor",
              },{
                "nome": "acoes",
                "label": "Ações",
                "values":[
                  {acao: "editar", label: "Editar Cor"},
                  {acao: "excluir", label: "Excluir Cor"}
                ]
              }]
    }

    static async get(page = 1){
      const result = await this.query().whereNull('deleted_at').paginate(page ? page.page : 1, 10)
      return result.toJSON()
    }

    static async getSite(){
      return this.query().whereNull('deleted_at').fetch()
    }

    static async getOptions(){
      const items = await this.query().whereNull('deleted_at').fetch()
      const serialize = items.toJSON()

      return serialize.map( i => { return ( {id: i.id, label:i.nome} ) } )
    }
}

module.exports = Cor
