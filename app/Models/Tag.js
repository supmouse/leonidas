'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Tag extends Model {
    static get table () {
        return 'ed_tags'
    }

    static get hidden () {
      return ['updated_at','created_at','deleted_at']
    }

    static async get(){
      return this.all()
    }

    static async getOptions(){
      const items = await this.all()
      const serialize = items.toJSON()

      return serialize.map( i => { return ( {id: i.id, label:i.nome} ) } )
    }
}

module.exports = Tag
