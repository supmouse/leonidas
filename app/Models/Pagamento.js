'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Pagamento extends Model {
    static get table () {
        return 'ec_pagamentos'
    }

    static get hidden () {
      return ['updated_at','created_at','deleted_at']
    }

}

module.exports = Pagamento
