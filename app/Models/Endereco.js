'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Endereco extends Model {
    static get table () {
        return 'gb_enderecos'
    }

    static get hidden () {
      return ['updated_at','created_at','deleted_at']
    }

    cidade () {
      return this.belongsTo('App/Models/Cidade', 'id_gb_cidade', 'id')
    }

    static async create(params) {

      const endereco = new Endereco()
      endereco.cep = params.cep
      endereco.id_gb_cidade = params.cidade
      endereco.rua = params.rua
      endereco.bairro = params.bairro
      endereco.numero = params.numero
      endereco.complemento = params.complemento
      endereco.id_usr_perfil = params.perfil
      await endereco.save()

      return endereco
    }

}

module.exports = Endereco
