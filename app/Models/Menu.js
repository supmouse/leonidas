'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const Database = use('Database')

class Menu extends Model {
    static get table () {
        return 'sc_menus'
    }
    
    static get hidden () {
      return ['updated_at','created_at','deleted_at']
    }


    static async getValues(id){
        return await Database.table(this.table).where('id_sc_menu',id).whereNull('deleted_at').orderBy('ordem')
    }

    static async get(){
        let r = await Database.table(this.table).whereNull('id_sc_menu').whereNull('deleted_at').orderBy('ordem')

        for(let i = 0; i < r.length; i++) {
            r[i].values = await this.getValues(r[i].id)

            for(let j = 0; j < r[i].values.length; j++) {
              let submenu = r[i].values[j]

              if(submenu.type === "submenu") {
                submenu.values = await this.getValues(submenu.id)
              }
            }
        }

        return r
    }
}

module.exports = Menu
