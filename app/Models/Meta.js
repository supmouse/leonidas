'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const Perfil = use('App/Models/Perfil')

class Meta extends Model {
  static get table () {
    return 'usr_meta'
  }

  static get hidden () {
    return ['updated_at','created_at','deleted_at']
  }

  static get deleteTimestamp () {
    return 'deleted_at'
  }

  static get listOptions () {
    return [{
              "nome": "id",
              "label": "#",
            },{
              "nome": "peso",
              "label": "Peso",
            },{
              "nome": "dt_final",
              "label": "Data Final",
            },{
              "nome": "acoes",
              "label": "Ações",
              "values":[
                {acao: "editar", label: "Editar Meta"},
                {acao: "excluir", label: "Excluir Meta"}
              ]
            }]
  }

  perfil () {
    return this.belongsTo('App/Models/Perfil', 'id_usr_perfil', 'id')
  }

  meta () {
    return this.belongsTo('App/Models/Meta', 'id_usr_meta', 'id')
  }

  static async get(page = 1){
    const result = await this.query().whereNull('deleted_at').paginate(page ? page.page : 1, 10)
    return result.toJSON()
  }

  static async getSite(page = 1, filters, order = null){
    const query = this.query()
    let chave = null
    let valor = null

    for (var f in filters) { 
      chave = filters[f].chave
      valor = filters[f].valor

        query.whereHas(chave, (builder) => {
      		builder.where('id', valor)
      	}, '>', 0)
      
    }


    const result = await query.whereNull('deleted_at').fetch()

    return result.toJSON()
  }

  static async create(params) {
    const meta = new Meta()
    const date = new Date()
    const finalDate = new Date()

    finalDate.setMonth(finalDate.getMonth() + params.meses)

    try {
      meta.peso = params.peso
      meta.dt_final = finalDate.getFullYear() +"-"+ (finalDate.getMonth()+1) +"-"+finalDate.getDate()
      meta.id_usr_perfil = params.id_usr_perfil
      await meta.save()

      const perfil = await Perfil.find(params.id_usr_perfil) 
      const diferenca = perfil.peso > params.peso ? perfil.peso - params.peso : params.peso - perfil.peso 
      const semanas = params.meses * 4
      const pesoSemana = diferenca / semanas

      for (var i = 0; i < semanas; i++){
        date.setDate(date.getDate() + 7)

        const metaf = new Meta()
        metaf.peso = perfil.peso > params.peso ? perfil.peso - (pesoSemana * (i+1)) : perfil.peso + (pesoSemana * (i+1))
        metaf.dt_final = date.getFullYear() +"-"+ (date.getMonth()+1) +"-"+date.getDate()
        metaf.id_usr_perfil = params.id_usr_perfil
        metaf.id_usr_meta = meta.id
        await metaf.save()
      }

    } catch (e) {
      console.log(e)
    }


    return meta
  }
}

module.exports = Meta
