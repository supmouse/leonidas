'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Highlight extends Model {
    static get table () {
        return 'ed_highlight'
    }

    static get hidden () {
      return ['updated_at','created_at','deleted_at']
    }

    static get deleteTimestamp () {
      return 'deleted_at'
    }

    static get getRelations (){
      const relations = [];
      relations["id_gb_imagem"] = "imagem"

      return relations
    }

    static get listOptions () {
      return [{
                "nome": "id",
                "label": "#",
              },{
                "nome": "id_gb_imagem",
                "label": "Imagem",
                "obj": "imagem",
              },{
                "nome": "titulo",
                "label": "Titulo",
              },{
                "nome": "acoes",
                "label": "Ações",
                "values":[
                  {acao: "editar", label: "Editar Highlight"},
                  {acao: "excluir", label: "Excluir Highlight"}
                ]
              }]
    }

    imagem () {
      return this.belongsTo('App/Models/Imagem', 'id_gb_imagem', 'id')
    }

    static label() {
      return this.nome
    }

    static async getValues(id){
        return await this.query().whereNull('deleted_at').fetch()
    }

    static async get(page = 1){
      let valor = await this.query().whereNull('deleted_at').paginate(page ? page.page : 1, 10)
      valor = valor.toJSON()

      return valor
    }

    static async getSite(){
      let valor = await this.query().whereNull('deleted_at').fetch()
      valor = valor.toJSON()
      return valor
    }

    static async getOptions(){
      const items = await this.query().whereNull('deleted_at').fetch()
      const serialize = items.toJSON()

      return serialize.map( i => { return ( {id: i.id, label:i.titulo} ) } )
    }
}

module.exports = Highlight
