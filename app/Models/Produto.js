'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Produto extends Model {
  static get table () {
    return 'ec_produtos'
  }

  static get hidden () {
    return ['updated_at','created_at','deleted_at']
  }

  static get deleteTimestamp () {
    return 'deleted_at'
  }

  static get total() {
      return this.valor - this.desconto()
  }

  static get desconto() {
      return this.valor * this.desconto/100
  }

  static get fildInfo () {
    const arr = [];
    arr["nome"] = "info do nome"
    arr["sku"] = "info sku"

    return arr
  }

  static get listOptions () {
    return [{
              "nome": "id",
              "label": "#",
            },{
              "nome": "id_gb_imagem",
              "label": "Imagem",
              "obj": "imagem",
            },{
              "nome": "nome",
              "label": "Nome",
            },{
              "nome": "descricao",
              "label": "Descrição",
            },{
              "nome": "valor",
              "label": "Valor",
            },{
              "nome": "id_categoria",
              "label": "Categoria",
              "obj": "categoria",
            },{
              "nome": "acoes",
              "label": "Ações",
              "values":[
                {acao: "editar", label: "Editar Produto"},
                {acao: "excluir", label: "Excluir Produto"}
              ]
            }]
  }

  static get getManyToMany (){
   
  }

  static get getRelations (){
    const relations = [];
    relations["id_categoria"] = "categoria"

    return relations
  }

 
  imagem () {
    return this.hasOne('App/Models/Imagem','id_gb_imagem','id')
  }

  categoria () {
    return this.hasOne('App/Models/Categoria','id_categoria','id')
  }

  static async get(page = 1, filtro = null){
    let result = await this.query().whereNull('deleted_at').orderBy("id", "desc").paginate(page ? page.page : 1, 10)
    
    if (filtro)
      result  = await this.query().where('id_categoria',filtro).whereNull('deleted_at').orderBy("id", "desc").paginate(page ? page.page : 1, 10)
    

    return result.toJSON()
  }

  static async getSite(){
    const result = await this.query().with('categoria').with('imagem').whereNull('deleted_at').fetch()

    return result.toJSON()
  }
}

module.exports = Produto
