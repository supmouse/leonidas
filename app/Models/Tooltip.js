'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Tooltip extends Model {
  static get table () {
    return 'gb_tooltips'
  }

  static get hidden () {
    return ['updated_at','created_at','deleted_at']
  }

  static async getInfo(model, coluna){
    return await this.query().where('model',model).where('coluna',coluna).first()
  }
}

module.exports = Tooltip
