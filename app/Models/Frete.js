'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Frete extends Model {
    static get table () {
        return 'ec_fretes'
    }

    static get hidden () {
      return ['updated_at','created_at','deleted_at']
    }

    static async create(params) {

      const frete = new Frete()
      frete.tipo = params.tipo
      frete.valor = params.valor
      await frete.save()

      return frete
    }

}

module.exports = Frete
