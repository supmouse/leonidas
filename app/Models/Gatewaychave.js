'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Gatewaychave extends Model {
    static get table () {
        return 'sc_gatewaychave'
    }

    static get hidden () {
      return ['updated_at','created_at','deleted_at']
    }

    static get deleteTimestamp () {
      return 'deleted_at'
    }

    static get listOptions () {
      return [{
                "nome": "id",
                "label": "#",
              },{
                "nome": "tipo",
                "label": "Tipo",
              },{
                "nome": "chave",
                "label": "Chave",
              },{
                "nome": "pais",
                "label": "País",
              },{
                "nome": "acoes",
                "label": "Ações",
                "values":[
                  {acao: "editar", label: "Editar Configuração"},
                  {acao: "excluir", label: "Excluir Configuração"}
                ]
              }]
    }

    static async get(page = 1){
      const result = await this.query().whereNull('deleted_at').paginate(page ? page.page : 1, 10)
      return result.toJSON()
    }

}

module.exports = Gatewaychave
