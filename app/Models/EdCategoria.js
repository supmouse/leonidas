'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class EdCategoria extends Model {
    static get table () {
        return 'ed_categorias'
    }

    static get hidden () {
      return ['updated_at','created_at','deleted_at']
    }

    static get listOptions () {
      return [{
                "nome": "id",
                "label": "#",
              },{
                "nome": "nome",
                "label": "Nome",
              },{
                "nome": "slug",
                "label": "Slug",
              },{
                "nome": "acoes",
                "label": "Ações",
                "values":[
                  {acao: "editar", label: "Editar Categoria"},
                  {acao: "excluir", label: "Excluir Categoria"}
                ]
              }]
    }

    conteudo () {
      return this.hasMany('App/Models/Conteudo', 'id', 'id_ed_categoria')
    }

    imagem () {
      return this.belongsTo('App/Models/Imagem', 'id_gb_imagem', 'id')
    }

    static async get(page = 1){
      let valor = await this.query().with('conteudo').whereNull('deleted_at').paginate(page ? page.page : 1, 10)
      valor = valor.toJSON()

      return valor
    }

    static async getSite(){
      const items = await this.query().with('imagem').with('conteudo').with('conteudo.imagem').whereNull('deleted_at').fetch()
      const serialize = items.toJSON()

      return serialize
    }

    static async getOptions(){
      const items = await this.query().whereNull('deleted_at').fetch()
      const serialize = items.toJSON()

      return serialize.map( i => {
        let categName = i.nome

        return ( {id: i.id, label:categName} )
      } )
    }
}

module.exports = EdCategoria
