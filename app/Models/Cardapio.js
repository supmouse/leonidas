'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Cardapio extends Model {
    static get table () {
        return 'Cardapio'
    }

    static get hidden () {
        return ['created_at', 'updated_at', 'deleted_at']
    }

    static async getValues(id){
        return await this.query().whereNull('deleted_at').fetch()
    }

    static async get(page = 1){
      let valor = await this.query().whereNull('deleted_at').paginate(page ? page.page : 1, 10)
      valor = valor.toJSON()

      return valor
    }

    static async getSite(){
      let valor = await this.query().whereNull('deleted_at').fetch()
      valor = valor.toJSON()
      return valor
    }

    static async getOptions(){
      const items = await this.query().whereNull('deleted_at').fetch()
      const serialize = items.toJSON()

      return serialize.map( i => { return ( {id: i.id, label:i.titulo} ) } )
    }
}

module.exports = Cardapio
