'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Paciente extends Model {
  static get table () {
    return 'usr_perfis'
  }

  static get hidden () {
    return ['updated_at','created_at','deleted_at']
  }

}

module.exports = Paciente
