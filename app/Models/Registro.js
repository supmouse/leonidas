'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Registro extends Model {
  static get table () {
    return 'usr_registros'
  }

  static get hidden () {
    return ['updated_at','created_at','deleted_at']
  }

  static get deleteTimestamp () {
    return 'deleted_at'
  }

  static get listOptions () {
    return [{
              "nome": "id",
              "label": "#",
            },{
              "nome": "nome",
              "label": "Nome",
            },{
              "nome": "valor",
              "label": "Valor",
            },{
              "nome": "acoes",
              "label": "Ações",
              "values":[
                {acao: "editar", label: "Editar Registro"},
                {acao: "excluir", label: "Excluir Registro"}
              ]
            }]
  }

  getPerfil () {
    return this.belongsTo('App/Models/Perfil', 'id_usr_perfil', 'id')
  }

  static async get(page = 1){
    const result = await this.query().whereNull('deleted_at').paginate(page ? page.page : 1, 10)
    return result.toJSON()
  }

  static async getSite(page = 1, filters, order = null){
    const query = this.query()

    const result = await query.whereNull('deleted_at').paginate(page ? page.page : 1, 10)

    return result.toJSON()
  }

  static async create(params) {
    const registro = new Registro()

    try {
      registro.peso = params.peso,
      registro.id_usr_perfil = params.user
      await registro.save()
    } catch (e) {
      console.log(e)
    }


    return registro
  }
}

module.exports = Registro
