'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Categoria extends Model {
    static get table () {
        return 'ec_categorias'
    }

    static get hidden () {
      return ['updated_at','created_at','deleted_at']
    }

    static get deleteTimestamp () {
      return 'deleted_at'
    }

    static get getRelations (){
      const relations = [];
      //relations["id_categoria"] = "categoria"

      return relations
    }

    static get listOptions () {
      return [{
                "nome": "id",
                "label": "#",
              },{
                "nome": "icon",
                "label": "Icone",
              },{
                "nome": "nome",
                "label": "Nome",
              },{
                "nome": "acoes",
                "label": "Ações",
                "values":[
                  {acao: "editar", label: "Editar Categoria"},
                  {acao: "excluir", label: "Excluir Categoria"}
                ]
              }]
    }

    imagem () {
      return this.belongsTo('App/Models/Imagem', 'id_gb_imagem', 'id').whereNull('deleted_at')
    }

    produto () {
      return this.belongsTo('App/Models/Produto', 'id', 'id_categoria').whereNull('deleted_at')
    }

    categoria () {
      return this.hasMany('App/Models/Categoria', 'id', 'id_categoria').whereNull('deleted_at')
    }

    static label() {
      return this.nome
    }

    static async get(page = 1){
      let valor = await this.query().whereNull('deleted_at').paginate(page ? page.page : 1, 10)
      valor = valor.toJSON()

      return valor
    }

    static async getSite(){
      let valor = await this.query().with('imagem').with('categoria').whereNull('id_categoria').whereNull('deleted_at').fetch()
      valor = valor.toJSON()

      return valor
    }

    static async getOptions(){
      const items = await this.query().whereNull('deleted_at').fetch()
      const serialize = items.toJSON()

      return serialize.map( i => {
        let categName =  i.nome

        return ( {id: i.id, label:categName} )
      } )
    }
}

module.exports = Categoria
