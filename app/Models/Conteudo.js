'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Conteudo extends Model {
    static get table () {
        return 'ed_conteudos'
    }

    static get hidden () {
      return ['updated_at','created_at','deleted_at']
    }

    static get listOptions () {
      return [{
                "nome": "id",
                "label": "#",
              },{
                "nome": "titulo",
                "label": "Titulo",
              },{
                "nome": "id_ed_categoria",
                "label": "Categoria",
              },{
                "nome": "acoes",
                "label": "Ações",
                "values":[
                  {acao: "editar", label: "Editar Conteudo"},
                  {acao: "excluir", label: "Excluir Conteudo"}
                ]
              }]
    }

    getManyToMany (){
      return ['tag']
    }

    tag () {
      return this.belongsToMany('ed_tags_has_ed_conteudos','id','id_ed_conteudo')
    }

    categoria () {
      return this.belongsTo('App/Models/EdCategoria', 'id_ed_categoria', 'id')
    }

    imagem () {
      return this.belongsTo('App/Models/Imagem', 'id_gb_imagem', 'id')
    }


    static async get(page = 1){
      let valor = await this.query().with('imagem').with('categoria').with('categoria.imagem').whereNull('deleted_at').paginate(page ? page.page : 1, 10)
      valor = valor.toJSON()

      return valor
    }

    static async getSite(){
      const items = await this.query().with('imagem').with('categoria').with('categoria.imagem').whereNull('deleted_at').fetch()
      const serialize = items.toJSON()

      return serialize
    }

    

    static async getSingle(value){
      return await this.query()
        .with('imagem')
        .with('categoria')
        .with('categoria.imagem')
        .where('id', value).whereNull('deleted_at').first()
    }

}

module.exports = Conteudo
