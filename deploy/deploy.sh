#!/bin/bash

set -e

echo -- Export name project variable
export PROJECT_NAME="leonidas"

echo -- Initial clone repository address
GITLAB_REPO="git@gitlab.com:supmouse/leonidas.git"

echo ---- Iniciando deploy ----

echo -- Desabilita validacao de Host --
mkdir -p ~/.ssh
touch ~/.ssh/config
echo -e "Host *\n\tStrictHostKeyChecking no\n\n" >> ~/.ssh/config

echo -- Gerando certificado para login na AWS --
touch key.pem
echo -e "$SERVER_SSH_KEY_PRD" > key.pem
chmod 600 key.pem

echo -- Acessando a AWS --
ssh -i key.pem -tt $SSH_USER@$SERVER_PRD << EOF

echo -- Verificando se projeto já foi clonado
if [[ ! -e "$PROJECT_NAME" ]]; then
    git clone $GITLAB_REPO $PROJECT_NAME
fi

echo -- Acessando diretorio do projeto --
cd "$PROJECT_NAME"

echo -- Atualizando projeto --
git pull

echo -- Parando pm2 --
pm2 restart 1

echo -- Deploy finalizado --
exit

EOF
